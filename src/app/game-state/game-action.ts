import { GameState, OverallState, Square } from './game-state';
import { GameStateInitializer } from './game-state-initializer';


/** Represents an action of the game. */
export abstract class GameAction {

  /**
   * Executes this GameAction on given GameState. This modifies the GameState object! Returns NotAllowedByRules if the action is not
   * allowed.
   */
  public abstract executeOn(gameState: GameState): void | NotAllowedByRules;

}


/**
 * Instance of this class is returned when player tries to execute Game Action that can't be executed in current Game State because it is
 * against the rules.
 * You must provide a message that can be displayed to the player that explains why the action can't be done.
 */
export class NotAllowedByRules {

  public constructor(private _userFriendlyMessage: string) { }

  /** Text of the message that will be displayed to the user. */
  public get userFriendlyMessage() { return this._userFriendlyMessage; }

}


/**
 * Player clicks a square to be revealed. This may represent two possible sub-actions:
 * a) Player clicked unrevealed square - dig here.
 * b) Player clicked revealed square - if there are exactly that many flags around the number, reveal all adjacent squares.
 */
export class RevealSquareAction extends GameAction {

  /** Array of offsets to get the squares around this square. */
  private static adjacentSquares: Array<[number, number]> = [[-1, -1], [-1, 0], [-1, 1], [0, -1], [0, 1], [1, -1], [1, 0], [1, 1]];

  public constructor(public lineId: number, public columnId: number) {
    super();
  }

  public executeOn(gameState: GameState): void | NotAllowedByRules {
    if (gameState.overallState === OverallState.Won || gameState.overallState === OverallState.Lost) {
      return new NotAllowedByRules('Game is over');
    }

    const square = gameState.grid[this.lineId][this.columnId];
    if (square.flag) {
      return new NotAllowedByRules('You can\'t dig on a flagged square');
    }

    if (square.revealed) {
      return this.digAllAdjacent(gameState, square);
    } else {
      this.dig(gameState, square);
    }

    // If we haven't exploded, check whether we won
    if (gameState.overallState === OverallState.Playing
      && gameState.revealedTotal + gameState.bombsTotal === gameState.lines * gameState.columns) {
      gameState.overallState = OverallState.Won;
    }
  }

  private dig(gameState: GameState, square: Square) {
    // Exactly after first click, place mines
    if (gameState.overallState === OverallState.New) {
      const initializer = new GameStateInitializer();
      initializer.placeMines(gameState, this.lineId, this.columnId);
    }

    square.revealed = true;
    gameState.revealedTotal++;

    // Check whether we explode
    if (square.mine) {
      gameState.overallState = OverallState.Lost;
    }

    // Count the number
    square.value = 0;
    this.forEachAdjacentSquare(gameState, (lineIdNew, columnIdNew) => {
      if (gameState.grid[lineIdNew][columnIdNew].mine) {
        square.value++;
      }
    });

    // In case of zero, recursively reveal all near squares.
    if (square.value === 0) {
      this.forEachAdjacentSquare(gameState, (lineIdNew, columnIdNew) => {
        if (!gameState.grid[lineIdNew][columnIdNew].revealed) {
          new RevealSquareAction(lineIdNew, columnIdNew).executeOn(gameState);
        }
      });
    }
  }

  private digAllAdjacent(gameState: GameState, square: Square): void | NotAllowedByRules {
    let flags = 0;
    this.forEachAdjacentSquare(gameState, (lineIdNew, columnIdNew) => {
      if (gameState.grid[lineIdNew][columnIdNew].flag) {
        flags++;
      }
    });

    if (flags !== square.value) {
      return new NotAllowedByRules('Number of flags on adjacent squares doesn\'t match this square\'s value');
    }

    this.forEachAdjacentSquare(gameState, (lineIdNew, columnIdNew) => {
      if (!gameState.grid[lineIdNew][columnIdNew].revealed) {
        new RevealSquareAction(lineIdNew, columnIdNew).executeOn(gameState);
      }
    });
  }

  private forEachAdjacentSquare(gameState: GameState, consumer: (lineIdNew: number, columnIdNew: number) => void) {
    for (const offset of RevealSquareAction.adjacentSquares) {
      const lineIdNew = this.lineId + offset[0];
      const columnIdNew = this.columnId + offset[1];
      if (lineIdNew >= 0 && lineIdNew < gameState.lines && columnIdNew >= 0 && columnIdNew < gameState.columns) {
        consumer(lineIdNew, columnIdNew);
      }
    }
  }

}


/** Player adds/removes flag from a square. */
export class ToggleFlagAction extends GameAction {

  public constructor(public lineId: number, public columnId: number) {
    super();
  }

  public executeOn(gameState: GameState): void | NotAllowedByRules {
    if (gameState.overallState === OverallState.Won || gameState.overallState === OverallState.Lost) {
      return new NotAllowedByRules('Game is over');
    }

    const square = gameState.grid[this.lineId][this.columnId];
    if (square.revealed) {
      return new NotAllowedByRules('You can\'t toggle flag on a square that is already revealed');
    }

    square.flag = !square.flag;
    gameState.flagsTotal += square.flag ? 1 : -1;
  }

}
