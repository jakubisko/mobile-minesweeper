import { GameState, Square, GameSettings, OverallState } from './game-state';

/**
 * Initializes GameState.
 * In Minesweeper, this is done only after the first action.
 */
export class GameStateInitializer {

  /**
   * Modifies given GameState to represent a clear board as defined by given GameSettings. No fields will be revealed and no mines placed.
   * This sets GameState's OverallState to New.
   */
  public clearGameState(gameState: GameState, settings: GameSettings) {

    const grid: Square[][] = [];

    for (let lineId = 0; lineId < settings.lines; lineId++) {
      const line: Square[] = [];
      for (let columnId = 0; columnId < settings.columns; columnId++) {
        const square: Square = {
          mine: false,
          value: 0,
          flag: false,
          revealed: false
        };
        line.push(square);
      }
      grid.push(line);
    }

    gameState.overallState = OverallState.New;
    gameState.lines = settings.lines;
    gameState.columns = settings.columns;
    gameState.bombsTotal = settings.bombsTotal;
    gameState.flagsTotal = 0;
    gameState.revealedTotal = 0;
    gameState.grid = grid;
  }

  /**
   * Modifies given GameState to represent situation after first click. This sets GameState's OverallState to Playing.
   */
  public placeMines(gameState: GameState, firstClickLineId: number, firstClickColumnId: number) {

    // Collect list of coordinates for each square except for the clicked square.
    const coordinates: Coordinate[] = [];
    for (let lineId = 0; lineId < gameState.lines; lineId++) {
      for (let columnId = 0; columnId < gameState.columns; columnId++) {
        if (Math.abs(lineId - firstClickLineId) > 1 || Math.abs(columnId - firstClickColumnId) > 1) {
          coordinates.push({
            lineId: lineId,
            columnId: columnId
          });
        }
      }
    }

    // Randomly remove elements from the list until "bombsTotal" items were chosen.
    let coordinatesLeft = coordinates.length;
    for (let i = 0; i < gameState.bombsTotal; i++) {
      const index = Math.floor(coordinatesLeft * Math.random());
      const coord = coordinates[index];
      gameState.grid[coord.lineId][coord.columnId].mine = true;
      coordinates[index] = coordinates[--coordinatesLeft];
    }

    gameState.overallState = OverallState.Playing;
  }

}

class Coordinate {
  lineId: number;
  columnId: number;
}
