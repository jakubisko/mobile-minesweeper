import { Injectable } from '@angular/core';
import { GameState, GameSettings } from './game-state';
import { GameStateInitializer } from './game-state-initializer';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class GameStateService {

  public static readonly possibleGameSettings: GameSettings[] = [{
    name: 'Beginner',
    lines: 8,
    columns: 8,
    bombsTotal: 10
  }, {
    name: 'Intermediate',
    lines: 16,
    columns: 16,
    bombsTotal: 40
  }, {
    name: 'Expert',
    lines: 16,
    columns: 30,
    bombsTotal: 99
  }];

  /** If you want to receive events on whether player is currently pressing uncovered square to display scared face, subscribe to this. */
  public scared = new BehaviorSubject<boolean>(false);

  /** If you want to receive events when new game starts, subscribe to this. */
  public newGameStarted = new BehaviorSubject<void>(null);

  private _gameState: GameState = new GameState();
  private lastGameSettings = GameStateService.possibleGameSettings[0];

  public constructor() {
    this.newGame();
  }

  public newGame(gameSettings?: GameSettings) {
    if (gameSettings === undefined) {
      gameSettings = this.lastGameSettings;
    } else {
      this.lastGameSettings = gameSettings;
    }

    new GameStateInitializer().clearGameState(this._gameState, gameSettings);
    this.newGameStarted.next(null);
  }

  public get gameState(): GameState {
    return this._gameState;
  }

  public getLastGameSettings(): GameSettings {
    return this.lastGameSettings;
  }

}
