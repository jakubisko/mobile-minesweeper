
/** Player can choose these before game starts. Defines rules under which GameState will be initialized. */
export class GameSettings {
  name: string;
  lines: number;
  columns: number;
  bombsTotal: number;
}

/** Whether game has started, is won, lost.. */
export enum OverallState {
  New, // No squares revealed. No mines placed yet.
  Playing, // Middle of game
  Won, // Player won.
  Lost // Player lost
}

/**
 * Represents a state of the game.
 * This class is mutable!
 * This is a pure data object and it shouldn't have any methods.
 */
export class GameState {
  overallState: OverallState;
  lines: number;
  columns: number;
  bombsTotal: number; // How many bombs are there
  flagsTotal: number; // How many flags user has placed
  revealedTotal: number; // How many field are currently revealed
  grid: Square[][]; // First index is line number, second is column number
}

export class Square {
  mine: boolean;
  value: number;
  flag: boolean;
  revealed: boolean;
}
