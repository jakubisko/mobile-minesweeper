import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

/** Timer will not go over this value. Initial high scores will have this value. */
export const maxTime = 999;

@Injectable({
  providedIn: 'root'
})
export class GameTimerService {

  /** If you want to receive game time, subscribe to this. */
  public gameTime = new BehaviorSubject<number>(0);

  private timer: NodeJS.Timer = null;

  /** Sets game time to zero. */
  public resetGameTime() {
    this.gameTime.next(0);
  }

  /** Starts game time. Doesn't set it to zero. */
  public startGameTime() {
    if (this.timer == null) {
      this.timer = setInterval(() => {
        const current = this.gameTime.getValue();

        if (current >= maxTime) {
          this.stopGameTime();
        } else {
          this.gameTime.next(current + 1);
        }
      }, 1000);
    }
  }

  /** Stops game time. Doesn't set it to zero. */
  public stopGameTime() {
    if (this.timer != null) {
      clearInterval(this.timer);
      this.timer = null;
    }
  }

}
