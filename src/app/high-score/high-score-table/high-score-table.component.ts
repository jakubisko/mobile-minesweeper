import { Component } from '@angular/core';
import { NgbModal, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { HighScoreService } from '../high-score.service';
import { HighScores } from '../high-score';

@Component({
  templateUrl: './high-score-table.component.html',
  styleUrls: ['./high-score-table.component.css']
})
export class HighScoreTableComponent {

  highScores: HighScores;

  public constructor(public activeModal: NgbActiveModal, private highScoreService: HighScoreService) {
    this.highScores = highScoreService.getHighScores();
  }

  public static openModal(modalService: NgbModal) {
    modalService.open(HighScoreTableComponent, { backdrop: 'static', centered: true });
  }

  resetHighScores() {
    this.highScoreService.resetHighScores();
    this.highScores = this.highScoreService.getHighScores();
  }

}
