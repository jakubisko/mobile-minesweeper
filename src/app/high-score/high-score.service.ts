import { Injectable } from '@angular/core';
import { HighScores, currentHighScoresVersion } from './high-score';
import { LocalStorage } from './local-storage';
import { maxTime } from '../game-timer/game-timer.service';
import { GameStateService } from '../game-state/game-state.service';

/**
 * Local storage is shared for all pages of same origin. Two applications deployed on localhost will share local
 * storage. Use prefix to differentiate between them.
 */
const LOCAL_STORAGE_KEY = 'MOBILE_MINESWEEPER_HIGHSCORE';

@Injectable({
  providedIn: 'root'
})
export class HighScoreService {

  private localStorage = new LocalStorage<HighScores>(LOCAL_STORAGE_KEY, currentHighScoresVersion);

  /** Returns whether browser supports saving high score. */
  public isSavingAvailable(): boolean {
    return this.localStorage.isStorageAvailable();
  }

  public getHighScores(): HighScores {
    return this.localStorage.readFromStorage() || this.generateDefaultHighScores();
  }

  private generateDefaultHighScores(): HighScores {
    const highScores: HighScores = { items: [] };
    for (const gameSettings of GameStateService.possibleGameSettings) {
      highScores.items.push({
        settingName: gameSettings.name,
        name: 'Anonymous',
        time: maxTime
      });
    }
    return highScores;
  }

  public resetHighScores() {
    this.localStorage.delete();
  }

  /** Returns whether given time is enough to be added to high scores. */
  public eligibleForHighScore(time: number, settingName: string): boolean {
    const highScores = this.getHighScores();
    return highScores.items.find(item => item.settingName === settingName).time > time;
  }

  /** Adds a new time to high scores. Does nothing if the new high score is not eligible. */
  public addHighScore(name: string, newTime: number, settingName: string) {
    if (!this.eligibleForHighScore(newTime, settingName)) {
      return;
    }

    const highScores = this.getHighScores();
    const highScoreItem = highScores.items.find(item => item.settingName === settingName);
    highScoreItem.name = name;
    highScoreItem.time = newTime;

    this.localStorage.writeToStorage(highScores);
  }

}
