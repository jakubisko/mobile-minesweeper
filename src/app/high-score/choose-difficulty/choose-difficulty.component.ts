import { Component } from '@angular/core';
import { NgbModal, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { GameStateService } from 'src/app/game-state/game-state.service';
import { GameSettings } from 'src/app/game-state/game-state';

@Component({
  templateUrl: './choose-difficulty.component.html'
})
export class DifficultyComponent {

  readonly possibleGameSettings = GameStateService.possibleGameSettings;

  public constructor(public activeModal: NgbActiveModal, private gameStateService: GameStateService) {
  }

  public static openModal(modalService: NgbModal) {
    modalService.open(DifficultyComponent, { backdrop: 'static', centered: true });
  }

  newGame(gameSettings: GameSettings) {
    this.gameStateService.newGame(gameSettings);
    this.activeModal.close();
  }

}
