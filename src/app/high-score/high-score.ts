
/** Increase the version number whenever the class for persisting changes. */
export const currentHighScoresVersion = 2;

export interface HighScores {
  items: HighScoreItem[];
}

export interface HighScoreItem {
  settingName: string;
  name: string;
  time: number;
}
