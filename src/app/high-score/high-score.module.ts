import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { HighScoreTableComponent } from './high-score-table/high-score-table.component';
import { EnterNameComponent } from './enter-name/enter-name.component';
import { DifficultyComponent } from './choose-difficulty/choose-difficulty.component';

@NgModule({
    imports: [CommonModule, FormsModule],
    declarations: [HighScoreTableComponent, EnterNameComponent, DifficultyComponent]
})
export class HighScoreModule { }
