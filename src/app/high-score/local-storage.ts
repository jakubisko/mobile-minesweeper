
/**
 * Use this class to read and write object into local storage to persist data between sessions.
 *
 * Local storage is shared for all pages of same origin. Two applications deployed on localhost will share local
 * storage. Use unique localStorageKey to distinguish them.
 *
 * If you want to store multiple objects, create multiple instances of this class with different localStorageKeys.
 */
export class LocalStorage<T> {

  public constructor(private localStorageKey: string, private dataClassVersion: number) { }

  /** Returns whether data persistence to local storage is enabled. */
  public isStorageAvailable(): boolean {
    try {
      const storage = window.localStorage;
      const x = '__storage_test__';
      storage.setItem(x, x);
      storage.removeItem(x);
      return true;
    } catch (e) {
      return false;
    }
  }

  public readFromStorage(): T {
    const serialized = window.localStorage.getItem(this.localStorageKey);
    if (serialized !== null) {
      const dataWithVersion = JSON.parse(serialized);
      if (dataWithVersion.version === this.dataClassVersion) {
        return dataWithVersion.data;
      }
    }
    return null;
  }

  public delete() {
    window.localStorage.removeItem(this.localStorageKey);
  }

  public writeToStorage(data: T) {
    const dataWithVersion = {
      data: data,
      version: this.dataClassVersion
    };
    const serialized = JSON.stringify(dataWithVersion);
    window.localStorage.setItem(this.localStorageKey, serialized);
  }

}
