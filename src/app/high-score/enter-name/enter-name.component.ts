import { Component } from '@angular/core';
import { NgbModal, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  templateUrl: './enter-name.component.html'
})
export class EnterNameComponent {

  newName = '';
  time: number;
  settingName: string;

  private onSubmit: (newName: string) => void;

  public constructor(public activeModal: NgbActiveModal) { }

  public static openModal(modalService: NgbModal, time: number, settingName: string, onSubmit: (newName: string) => void) {
    const modal = modalService.open(EnterNameComponent, { backdrop: 'static', keyboard: false, centered: true })
      .componentInstance as EnterNameComponent;
    modal.onSubmit = onSubmit;
    modal.time = time;
    modal.settingName = settingName;
  }

  done() {
    this.activeModal.close();
    this.onSubmit(this.newName);
  }

}
