import { Component } from '@angular/core';
import { SwUpdate } from '@angular/service-worker';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

 constructor(private swUpdate: SwUpdate) {
    this.checkForUpdates();
  }

  private checkForUpdates() {
    this.swUpdate.available.subscribe(event => {
      if (confirm('There is a newer version of this app available. Do you want to reload?')) {
        this.swUpdate.activateUpdate().then(() => document.location.reload());
      }
    });
  }

  preventContextMenu() {
    return false;
  }

}
