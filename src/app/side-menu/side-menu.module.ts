import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SideMenuComponent } from './side-menu/side-menu.component';
import { CounterComponent } from './counter/counter.component';

@NgModule({
  imports: [CommonModule],
  exports: [SideMenuComponent],
  declarations: [SideMenuComponent, CounterComponent]
})
export class SideMenuModule { }
