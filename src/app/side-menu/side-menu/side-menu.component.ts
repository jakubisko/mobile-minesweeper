import { Component, OnInit, OnDestroy } from '@angular/core';
import { GameStateService } from '../../game-state/game-state.service';
import { GameState, OverallState } from '../../game-state/game-state';
import { HighScoreTableComponent } from '../../high-score/high-score-table/high-score-table.component';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { HighScoreService } from '../../high-score/high-score.service';
import { GameTimerService } from '../../game-timer/game-timer.service';
import { Subscription } from 'rxjs';
import { DifficultyComponent } from 'src/app/high-score/choose-difficulty/choose-difficulty.component';

@Component({
  selector: 'app-side-menu',
  templateUrl: './side-menu.component.html',
  styleUrls: ['./side-menu.component.css']
})
export class SideMenuComponent implements OnInit, OnDestroy {

  readonly OverallState = OverallState;

  gameState: GameState;
  gameTime = 0;
  isSavingAvailable: boolean;
  scared: boolean;

  private gameTimerServiceSubscription: Subscription;
  private scaredSubscription: Subscription;


  public constructor(private gameStateService: GameStateService, private highScoreService: HighScoreService,
    private modalService: NgbModal, private gameTimerService: GameTimerService) {
    this.gameState = gameStateService.gameState;
    this.isSavingAvailable = highScoreService.isSavingAvailable();
  }

  public ngOnInit() {
    this.gameTimerServiceSubscription = this.gameTimerService.gameTime.asObservable().subscribe(newTime => this.gameTime = newTime);
    this.scaredSubscription = this.gameStateService.scared.asObservable().subscribe(newScared => this.scared = newScared);
  }

  newGame() {
    if (this.gameState.overallState === OverallState.New) {
      DifficultyComponent.openModal(this.modalService);
    } else {
      this.gameStateService.newGame();
      this.gameTimerService.stopGameTime();
      this.gameTimerService.resetGameTime();
    }
  }

  displayHighScores() {
    if (this.highScoreService.isSavingAvailable()) {
      HighScoreTableComponent.openModal(this.modalService);
    }
  }

  public ngOnDestroy() {
    this.gameTimerServiceSubscription.unsubscribe();
    this.scaredSubscription.unsubscribe();
  }

}
