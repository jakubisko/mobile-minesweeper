import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { ServiceWorkerModule } from '@angular/service-worker';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { environment } from '../environments/environment';

import { AppComponent } from './app.component';
import { GameAreaModule } from './game-area/game-area.module';
import { SideMenuModule } from './side-menu/side-menu.module';
import { HighScoreModule } from './high-score/high-score.module';

@NgModule({
  declarations: [AppComponent],
  imports: [
    BrowserModule, NgbModule,
    ServiceWorkerModule.register('/ngsw-worker.js', { enabled: environment.production }),
    GameAreaModule, SideMenuModule, HighScoreModule
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
