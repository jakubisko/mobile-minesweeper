import { Component } from '@angular/core';
import { GameStateService } from '../../game-state/game-state.service';
import { GameState, OverallState } from '../../game-state/game-state';
import { RevealSquareAction, GameAction, ToggleFlagAction } from '../../game-state/game-action';
import { GameTimerService } from '../../game-timer/game-timer.service';
import { HighScoreService } from '../../high-score/high-score.service';
import { EnterNameComponent } from '../../high-score/enter-name/enter-name.component';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { HighScoreTableComponent } from '../../high-score/high-score-table/high-score-table.component';
import { BehaviorSubject } from 'rxjs';

@Component({
  selector: 'app-game-area',
  templateUrl: './game-area.component.html',
  styleUrls: ['./game-area.component.css']
})
export class GameAreaComponent {

  readonly OverallState = OverallState;

  gameState: GameState;
  lineIdPushed = -1;
  columnIdPushed = -1;

  private scared: BehaviorSubject<boolean>;

  public constructor(private gameStateService: GameStateService, private gameTimerService: GameTimerService,
    private highScoreService: HighScoreService, private modalService: NgbModal) {
    this.gameState = gameStateService.gameState;
    this.scared = gameStateService.scared;
  }

  onMouseDown(lineId: number, columnId: number, buttons: number) {
    if (buttons === 1) {
      this.pushSquare(lineId, columnId);
    }
  }

  onTouchStart(lineId: number, columnId: number) {
    this.pushSquare(lineId, columnId);
  }

  onTouchEnd() {
    // This handler is specifically for situation when user touches square, drags finger outside of square, then ends touch.
    // Timeout is required for situation when user clicks without dragging. UnpushSquare would occur before "onClick" or "onContextMenu";
    // effectively negating them.
    setTimeout(() => {
      this.unpushSquare();
    }, 20); // 1 doesn't work
  }

  onMouseLeave(buttons: number) {
    if (buttons !== 0) {
      this.unpushSquare();
    }
  }

  onClick(lineId: number, columnId: number) {
    this.executeActionCheckEndOfGame(new RevealSquareAction(lineId, columnId));
    this.unpushSquare();
  }

  onContextMenu(lineId: number, columnId: number) {
    this.executeActionCheckEndOfGame(new ToggleFlagAction(lineId, columnId));
    this.unpushSquare();
  }

  /** When user pushes down button, but does not yet release it, show scared face. */
  private pushSquare(lineId: number, columnId: number) {
    const square = this.gameState.grid[lineId][columnId];
    if ((this.gameState.overallState <= OverallState.Playing) && !square.flag && (!square.revealed || square.value > 0)) {
      this.lineIdPushed = lineId;
      this.columnIdPushed = columnId;
      this.scared.next(true);
    }
  }

  /** When user stops pushing button, because he clicked or moved somewhere else, hide scared face. */
  private unpushSquare() {
    this.lineIdPushed = -1;
    this.columnIdPushed = -1;
    this.scared.next(false);
  }

  private executeActionCheckEndOfGame(action: GameAction) {
    const oldState = this.gameState.overallState;
    action.executeOn(this.gameState);

    if (this.gameState.overallState !== oldState) {
      if (this.gameState.overallState === OverallState.Playing) {
        this.gameTimerService.startGameTime();
      }
      if (oldState === OverallState.Playing) {
        this.gameTimerService.stopGameTime();
      }
      if (this.gameState.overallState === OverallState.Won) {
        this.gameWon();
      }
    }
  }

  private gameWon() {
    const gameTime = this.gameTimerService.gameTime.getValue();
    const lastGameSettings = this.gameStateService.getLastGameSettings();
    if (this.highScoreService.isSavingAvailable() && this.highScoreService.eligibleForHighScore(gameTime, lastGameSettings.name)) {
      EnterNameComponent.openModal(this.modalService, gameTime, lastGameSettings.name, (name) => {
        this.highScoreService.addHighScore(name, gameTime, lastGameSettings.name);
        HighScoreTableComponent.openModal(this.modalService);
      });
    }
  }

}
