import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { GameAreaComponent } from './game-area/game-area.component';
import { ScrollPaneComponent } from './scroll-pane/scroll-pane.component';

@NgModule({
  imports: [CommonModule],
  exports: [ScrollPaneComponent],
  declarations: [GameAreaComponent, ScrollPaneComponent]
})
export class GameAreaModule { }
