import { Component, OnInit, ViewChild, ElementRef, HostListener, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { GameStateService } from 'src/app/game-state/game-state.service';

@Component({
  selector: 'app-scroll-pane',
  templateUrl: './scroll-pane.component.html',
  styleUrls: ['./scroll-pane.component.css']
})
export class ScrollPaneComponent implements OnInit, OnDestroy {

  @ViewChild('container') container: ElementRef;
  @ViewChild('content') content: ElementRef;

  contentLeft: number;
  contentTop: number;

  private newGameStarted: Subscription;

  public constructor(private gameStateService: GameStateService) { }

  public ngOnInit() {
    this.newGameStarted = this.gameStateService.newGameStarted.asObservable().subscribe(_ => setTimeout(() => this.recalculatePosition()));
  }

  @HostListener('window:resize')
  public recalculatePosition() {
    const containerWidth: number = this.container.nativeElement.clientWidth;
    const containerHeight: number = this.container.nativeElement.clientHeight;
    const contentWidth: number = this.content.nativeElement.clientWidth;
    const contentHeight: number = this.content.nativeElement.clientHeight;

    this.contentLeft = (containerWidth > contentWidth) ? (containerWidth - contentWidth) / 2 : 0;
    this.contentTop = (containerHeight > contentHeight) ? (containerHeight - contentHeight) / 2 : 0;
  }

  public ngOnDestroy() {
    this.newGameStarted.unsubscribe();
  }

}
