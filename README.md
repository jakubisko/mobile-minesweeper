# MobileMinesweeper

### npm i -g @angular/cli

Install the angular command line tools for building and running the app.

### ng serve

Run on a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

### ng generate component component-name

Generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

### ng build
### ng build --configuration="production"

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--configuration="production"` flag for a production build.

### firebase login:ci --interactive

Provides a link that will generate Oauth2 token needed for authentication to deploy on Firebase.
Token and instructions will be written into console.

### firebase deploy

Deploys the production build to Firebase server.

## Used angular libraries:

* Bootstrap - CSS Styles for HTML:
  http://getbootstrap.com/

* ng-bootstrap - Library for modal window:
  https://ng-bootstrap.github.io/#/components

* popper - Dependency for ng-bootstrap:
  https://www.npmjs.com/package/@popperjs/core
